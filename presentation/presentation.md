title: Introdução a Service Workers
class: animation-fade
layout: true


---

class: impact

<img src="https://www.laborit.com.br/static/media/logo.b6416fdd.svg" alt="Laborit Brazil - Logo" title="Lab - Create positive change to reinvent the world." class="sc-fjdhpX idyKKo">
<br />
<br />
<br />

# {{title}}

---
<!-- ---

class: impact-title

# Service Worker

--- -->

# What is a Service Worker (SW)?
.col-6[
<img src="https://i.pinimg.com/originals/a5/59/bc/a559bcd33ce3e8c994b04482166531aa.gif" style="width: 535px;">
]
.col-6[
  <br />
- A service worker (SW) is a new browser technology that’s enabling offline experiences on the web

- Provide app-like experiences within the browser

- Used to enable features such as periodic sync and geofencing
]


---

class: impact-title


# Where does SW come from?

  <br />
  <br />
**AppCache** an application caching mechanism that allows web-based applications to run offline.


---

# What are the use case for SW?

.col-7[
The benefits for **website users** include:
- **Faster load times**.
- **Offline browsing**. 

The **business benefits** of developing a service worker include:
- **Customised offline fallback experiences**.
- **Native-like experiences in-browser**.
- **User engagement**.
- **Load balancing**.
- **Granular cache control**.
- **No blocking**.

]

---

class: impact-title

# How to set up a SW

---


# Set up registration

.col-6[
Initially registered inside your JavaScript file:


```js
if('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/sw.js')
    .then(() => { 
      console.log("Service Worker Registered"); 
    });
  }
```

This is described the so-called scope `navigator.serviceWorker .register('./blog/service-worker.js')`.


]
---


.col-5[
# Install lifecycle service worker

After that, it is updated when:

- A navigation to an in-scope page occurs.
- An event is fired on the service worker and it hasn't been downloaded in the last 24 hours.

]

.col-7[
<img src="https://developers.google.com/web/fundamentals/primers/service-workers/images/sw-lifecycle.png?hl=pt-br" height="600" alt="Ciclo de vida do service worker">
]


---

# Install the service worker

After installing the service worker, files are added to the browser's cache storage:

.col-6[

```js
var cacheName = 'myFilesToCache-v1'; 
var filesToCache = [ 
	'./', './index.html', 
	'./scripts/app.js', 
	'./inline.css', 
	'./images/wind.png' 
]; 
self.addEventListener('install', (e) => { 
	console.log('[ServiceWorker] Install'); 
	e.waitUntil( 
		caches.open(cacheName).then((cache) => { 
			console.log('[ServiceWorker] Caching app shell'); 
			return cache.addAll(filesToCache);
	 	})
	 );
 });
```

]
---

# Activate

This step is used to remove old caches when there are new. Old caches will get identified by the name of a newer cache.

.col-6[
<img src="https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/images/cm-on-activate.png" height="300px">
]

.col-6[

```js
self.addEventListener('activate', (event) => {
  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames.filter((cacheName) => {
          // Return true if you want to remove this cache,
          // but remember that caches are shared across
          // the whole origin
        }).map((cacheName) => {
          return caches.delete(cacheName);
        })
      );
    })
  );
});
```
]


---

class: impact-title

# Requirements and restrictions

---

# Browsers Support
<br />
<br />
- [caniuse.com](https://caniuse.com/#feat=serviceworkers)
- [Is serviceworking ready?](https://jakearchibald.github.io/isserviceworkerready/)

---


# HTTPS

.col-6[

<br />
<br />
Because of [security reasons](https://w3c.github.io/webappsec-secure-contexts/#threat-risks), SW will just run on sites delivered over HTTPS. But it's also possible to run them on http://localhost for development purposes.
]

---


# Cache Limitation

The available storage capacity for the [SW is limited by the browser's](https://developers.google.com/web/ilt/pwa/live-data-in-the-service-worker#how_much_can_you_store) cache or quota API:

<br />
<br />
<img width="100%" alt="table showing available storage capacity for the SW" src="https://inviqa.com/sites/default/files/inline-images/image3_2.png">

---


# Scope

.col-6[

<br />
A service worker is only active for the user if they're in the right subset of the website's domain. A single SW can control many pages. Each time a page within your scope is loaded, the SW is installed against that page and operates on it.  

```js
navigator.serviceWorker.register('/service-worker.js', {
  scope: '/app/'
});
```
]

.col-6[


Service worker will control requests from pages like `/app/`, `/app/lower/` and `/app/lower/lower`, but not from pages like `/app` or `/`, which are higher.

If you want the service worker to control higher pages `/app` you can indeed change the scope option, but you'll also need to set the [Service-Worker-Allowed HTTP Header](https://w3c.github.io/ServiceWorker/#service-worker-script-response) in your server config for the request serving the service worker script.


```js
navigator.serviceWorker.register('/app/service-worker.js', {
  scope: '/app'
});
```

]

---

# User-centricity

.col-6[

<br />
<br />
A SW stays active until all tabs or windows with your web app or website are closed. This is to prevent data corruption and other negative experiences that could impact the browsing experience. When a user re-opens your web app a new SW can get active or before that have to wait.  
]


---

.col-4[
# Waiting

A new cache is opened on SW’s install event when there is an changed version number or different cache name: 

`var cacheName = 'weatherPWAs-v6'`

The new SW has to wait when this is happening:
]
.col-7[
<img alt="screenshot of an open SW debugging panel in the browser" src="https://inviqa.com/sites/default/files/inline-images/image4_1.png" height="500px">
]


---

# Security Considerations

- **Secure Context**.
- **Content Security Policy** (this restriction is to mitigate a broad class of content injection vulnerabilities, such as cross-site scripting (XSS)).
    - Origin Relativity. 
    - `importScripts()`.
- **Cross-Origin Resources and CORS**.


---

class: impact-title

# Debugging

---

# Debugging in Chrome

In the following example you can see how to open the application panel in the Chrome browser dev tool to get information about the running SW and what is already cached.

<img alt="screenshot showing debugging a SW in Chrome" src="https://inviqa.com/sites/default/files/inline-images/image5.gif" height="430px">

---

# Debugging in Firefox

<iframe width="900" height="460" src="https://www.youtube.com/embed/1FWUYHxt5W4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


---

class: impact-title

# The cache machine

---

# Segura a onda, cara

<img src="https://miro.medium.com/max/1400/1*B01Mqbn1INsfn1alwDpzQg.jpeg" height="430px">

---

# On install - as a dependency

.col-6[

````js
self.addEventListener('install', (event) => {
  event.waitUntil(async () => {
    const cache = await caches.open('mysite-static-v3');
    await cache.addAll([
      '/css/whatever-v3.css',
      '/css/imgs/sprites-v6.png',
      '/css/fonts/whatever-v8.woff',
      '/js/all-min-v4.js'
      // etc
    ]);
  }());
});
```
]

.col-6[
<img src="https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/images/cm-on-install-dep.png" height="300px">
]

---

# On install - not as a dependency

.col-6[

```js
self.addEventListener('install', (event) => {
  event.waitUntil(async () => {
    const cache = await caches.open('mygame-core-v1');
    cache.addAll(
      // levels 11-20
    );
    await cache.addAll(
      // core assets & levels 1-10
    );
  }());
});
```
]

.col-6[
<img src="https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/images/cm-on-install-not.png" height="300px">
]

---

# On user interaction

.col-6[
<img src="https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/images/cm-on-user-interaction.png" height="130px">
<br />


- [CacheStorage](https://developer.mozilla.org/en-US/docs/Web/API/CacheStorage)
]


.col-6[
```js
document
  .querySelector('.cache-article')
  .addEventListener('click', (event) => {
    event.preventDefault();

    var id = this.dataset.articleId;
    caches.open('mysite-article-' + id).then((cache) => {
      fetch('/get-article-urls?id=' + id).then((response) => {
        // /get-article-urls returns a JSON-encoded array of
        // resource URLs that a given article depends on
        return response.json();
      }).then((urls) => {
        cache.addAll(urls);
      });
    });
});
```
]



---

# On network response

.col-7[

```js
self.addEventListener('fetch', (event) => {
  event.respondWith(
    caches.open('mysite-dynamic').then((cache) => {
      return cache.match(event.request).then((response) => {
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      });
    })
  );
});
```
]

.col-5[
<img src="https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/images/cm-on-network-response.png" height="210px">

]
---
# On push message

.col-5[
<img src="https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/images/cm-on-push.png" height="400px">

]


---


.col-4[
# Push Messasing Flow
]

.col-8[
<img src="https://www.w3.org/TR/push-api/sequence_diagram.png" height="600" alt="Example flow of events for subscription, push message delivery, and unsubscription">
]


---



# On push message

[Offline Cookbook](https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/?hl=pt-BR#on-push-message)

.col-6[
```js
self.addEventListener('push', (event) => {
  if (event.data.text() == 'new-email') {
    event.waitUntil(
      caches.open('mysite-dynamic').then((cache) => {
        return fetch('/inbox.json').then((response) => {
          cache.put('/inbox.json', response.clone());
          return response.json();
        });
      }).then((emails) => {
        registration.showNotification("New email", {
          body: "From " + emails[0].from.name
          tag: "new-email"
        });
      })
    );
  }
});

```
]

.col-6[



```js
self.addEventListener('notificationclick', (event) => {
  if (event.notification.tag == 'new-email') {
    // Assume that all of the resources needed to render
    // /inbox/ have previously been cached, e.g. as part
    // of the install handler.
    new WindowClient('/inbox/');
  }
});
```
]



---

# On background-sync


<img src="https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/images/cm-on-bg-sync.png" height="200px">

[How to request a background sync](https://developers.google.com/web/updates/2015/12/background-sync)

```js
self.addEventListener('sync', (event) => {
  if (event.id == 'update-leaderboard') {
    event.waitUntil(
      caches.open('mygame-dynamic').then((cache) => {
        return cache.add('/leaderboard.json');
      })
    );
  }
});

```
---

# Cache persistence

<img src="https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/images/cm-on-bg-sync.png" height="200px">

```js
navigator.storageQuota.queryInfo("temporary").then((info) => {
  console.log(info.quota);
  // Result: <quota in bytes>
  console.log(info.usage);
  // Result: <used data in bytes>
});
```

---

class: impact-title

# Firebase Cloud Messaging

---


# FCM

.col-6[
<img src="https://firebase.google.com/docs/cloud-messaging/images/diagram-FCM.png" width="100%">
]
.col-6[
1 - Tooling to compose or build message requests. 

2 - Accepts message requests, performs fanout of messages via topics, and generates message metadata such as the message ID.

3 - The message to the targeted device, handles message delivery, and applies platform-specific configuration where appropriate.

4 - On the user’s device, where the notification is displayed or the message is handled according to the app’s foreground/background state and any relevant application logic.
]
---

# LifeCycle (FCM)

.col-7[
<img src="https://ahex.co/wp-content/uploads/2018/02/Send-SMS-using-Laravel-Plivo-copy-2.png" width="100%">
]
.col-5[
- **Register devices to receive messages from FCM**. 
(Registers to receive messages, obtaining a registration token that uniquely identifies the app instance)

- **Send and receive downstream messages**.
]

---

class: impact-title


obrigado.

thank you.

arigato.

gracias.

merci.

kanimambo.

ke a leboga.

nda pandula.

vlw flw.

---

# References

- [ServiceWorker Cookbook](https://serviceworke.rs/strategy-network-or-cache.html)
- [Secure Contexts](https://w3c.github.io/webappsec-secure-contexts/)
- [Service Workers Nightly](https://w3c.github.io/ServiceWorker/)
- [Service Workers: an Introduction
](https://developers.google.com/web/fundamentals/primers/service-workers)
- [Adding Push Notifications to a Web App](https://developers.google.com/web/fundamentals/codelabs/push-notifications)
- [Custom Load Balancing With Cloudflare Workers](https://blog.cloudflare.com/update-response-headers-on-cloudflare-workers/)
- [CacheStorage](https://developer.mozilla.org/en-US/docs/Web/API/CacheStorage)
- [RIP Tutorial - A simple service worker](https://riptutorial.com/javascript/example/11067/a-simple-service-worker)

---